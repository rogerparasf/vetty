package Vetty;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Vetty_launch
{
	WebDriver driver;
    WebDriverWait wait;
    Actions ac;
    JavascriptExecutor jse;
    WebElement psb_ac,sea1;
    //List <WebElement> segList;
	@BeforeTest
	public void Launch_case1()
	{
		//System.out.println("Start Test");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\win10\\Desktop\\VEET\\VEET\\Chrome driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		 wait = new WebDriverWait(driver, 20);
	     ac = new Actions(driver);
	     jse =  (JavascriptExecutor)driver;
	     
	}
@Test(priority=1)
 public void Login_case2() throws Exception
 {
	 driver.get("https://stgclient.vetty.co/client/login");
	 driver.findElement(By.xpath("//input[@id='email']")).sendKeys("vijay@tweeny.in");
     driver.findElement(By.xpath("//input[@id='ssn']")).sendKeys("123456");
     driver.findElement(By.xpath("//button[@class='form-control btn btn-primary']")).click();
	// System.out.println("Hello world");
	
	 
 } 
 
 @Test(priority=2)
 public void Create_case3() throws InterruptedException 
 {
	 Thread.sleep(2000);
     driver.findElement(By.xpath("//*[@class='client-nav-item-btn']")).click();
     Thread.sleep(2000);
     wait = new WebDriverWait(driver, 20);
     driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("amu");
     driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("Sharma");
     driver.findElement(By.xpath("//input[@id='email']")).sendKeys("aev@gmail.com");
     Thread.sleep(2000);
     Select sel = new Select(driver.findElement(By.xpath("//*[@formcontrolname='selectedPackage']")));	 
     sel.selectByVisibleText("Paintzen_Contractor_All_Check");
     driver.findElement(By.xpath("//button[contains(@class,'mx-auto btn-blue lg place-order')]")).click();
     Thread.sleep(5000);
     driver.quit();
 
 }
 

}
